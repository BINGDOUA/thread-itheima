package com.jk;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        new App().init();
    }

    private void init() {
        final Outputer outputer = new Outputer();
        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    outputer.output2("11111111111111");
                }
            }
        }).start();

        new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    outputer.output2("88888888888888");
                }
            }
        }).start();
    }

    static class Outputer {
        String xxx = "";

        public void output(String name) {
            int len = name.length();
            synchronized (xxx) {
                for (int i = 0; i < len; i++) {
                    System.out.print(name.charAt(i));
                }
                System.out.println("");
            }
        }

        public synchronized void output2(String name) {
            int len = name.length();
            //synchronized (xxx){
            for (int i = 0; i < len; i++) {
                System.out.print(name.charAt(i));
            }
            System.out.println("");
            //}
        }
        //冲突测试
    }
}
